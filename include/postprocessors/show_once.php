<?php
class show_once extends postprocessor
{
    public function do_postprocessing($s, $label)
    {
        if(!isset($_SESSION['presentation']))
        {
            $_SESSION['presentation'] = '1';
            return $s; // Show content once for first visit
        }
        else
        {
            return " ";
        }
    }

    public function get_name($lang)
    {
        return "show_once";
    }

    public function get_description($lang)
    {
        return "Вызов содержимого лишь один раз на сессию";
    }

}