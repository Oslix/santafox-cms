<?php

class Memcache {
  private $expire;
  private $cache;

  public function __construct($expire) {
	  
    $compressed = false;
    $this->expire = $expire;

    $this->cache = new Memcached();
    $this->cache->setOption(Memcached::OPT_LIBKETAMA_COMPATIBLE, true);
    
    if(CACHE_COMPRESSED)
		$compressed = true;
    
    $this->cache->setOption(Memcached::OPT_COMPRESSION, $compressed);
    $this->cache->addServer(CACHE_HOSTNAME, CACHE_PORT);
  }

  public function get($key) {
    $key_unique = $this->keyByNamespace($key);
    return $this->cache->get($key_unique);
  }

  public function set($key, $value) {
    $key_unique = $this->keyByNamespace($key);
    return $this->cache->set($key_unique, $value, $this->expire);
  }

  public function delete($key) {    
    $this->cache->delete(CACHE_PREFIX . $key, 0);
  }

  public function flush() {
    $this->cache->increment(CACHE_PREFIX . "_namespace");
  }

  private function counter() {
    $counter = $this->cache->get(CACHE_PREFIX . "_namespace");
    if($counter === false) {
      $counter = rand(1, 10000);
      $this->cache->set(CACHE_PREFIX . "_namespace", $counter);
    }
    return $counter;
  }

  private function keyByNamespace($key) {
    $counter = $this->counter();
    return CACHE_PREFIX . "_" . $counter . "_" . $key;
  }
}
